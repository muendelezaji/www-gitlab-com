---
release_number: "16.6" # version number - required
title: "GitLab 16.6 released with GitLab Duo Chat available in Beta" # short title (no longer than 62 characters) - required
author: Matthew Macfarlane # author name and surname - required
author_gitlab: mmacfarlane # author's gitlab.com username - required
image_title: '/images/16_6/16_6-cover-image.png' # cover image - required
description: "GitLab 16.6 released with MR approvals as a compliance policy, improved forking, improved UI for CI/CD variable management, switchboard portal for GitLab Dedicated and much more!" # short description - required
twitter_image: '/images/16_6/16_6-cover-image.png' # required - copy URL from image title section above
categories: releases # required
layout: release # required
featured: yes
rebrand_cover_img: true

# APPEARANCE
# header_layout_dark: true #uncomment if the cover image is dark
# release_number_dark: true #uncomment if you want a dark release number
# release_number_image: "/images/X_Y/X_Y-release-number-image.svg" # uncomment if you want a svg image to replace the release number that normally overlays the background image

---

<!--
This is the release blog post file. Add here the introduction only.
All remaining content goes into data/release-posts/.

**Use the merge request template "Release-Post", and please set the calendar due
date for each stage (general contributions, review).**

Read through the Release Posts Handbook for more information:
https://about.gitlab.com/handbook/marketing/blog/release-posts/#introduction
-->

Today, we are excited to announce the release of GitLab 16.6 with [GitLab Duo Chat Available in Beta](#gitlab-duo-chat-available-in-beta), [MR approvals as a compliance policy](#allow-users-to-enforce-mr-approvals-as-a-compliance-policy), [improved forking](#minimal-forking-only-include-the-default-branch), [improved UI for CI/CD variable management](#improved-ui-for-cicd-variable-management), and much more!

These are just a few highlights from the 25+ improvements in this release. Read on to check out all of the great updates below.

To the wider GitLab community, thank you for the 137 contributions you provided to GitLab 16.6!
At GitLab, [everyone can contribute](https://about.gitlab.com/community/contribute/) and we couldn't have done it without you!

To preview what's coming in next month’s release, check out our [Upcoming Releases page](/direction/kickoff/), which includes our 16.7 release kickoff video.
